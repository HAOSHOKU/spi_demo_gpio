// SPI speed: MHZ(real), KHZ(simulation)
//Slow 01: f=20khz => t=50us
//Slow 02: f=1khz => t=1000us
#define TFull 50
#define THalf TFull/2

#define SCK  4
#define MOSI 5
#define MISO 6
#define SS   7

#define SCK_OUTPUT    DDRD |= (1<<DDD4) // <=> pinMode(SCK_PIN, OUTPUT);
#define MOSI_OUTPUT   DDRD |= (1<<DDD5)
#define SS_OUTPUT     DDRD |= (1<<DDD7)
#define MISO_INPUT    DDRD &= ~(1<<DDD6) // <=> pinMode(MISO, INPUT);   // đúng

// #define MISO_HIGH     PORTD |= (1<<PD5) //<=> digitalWrite(MISO_HIGH,1);
// #define MISO_LOW   PORTD &= ~(1<<PD5) //<=> digitalWrite(MISO_LOW,0);

#define write_MOSI(x) PORTD = x ? (PORTD |= (1<<PD5)):(PORTD &= ~(1<<PD5)) //<=> digitalWrite(MOSI,1);
#define write_SCK(x)  PORTD = x ? (PORTD |= (1<<PD4)):(PORTD &= ~(1<<PD4))
#define write_SS(x)   PORTD =x ? PORTD |= (1<<PD7):(PORTD &= ~(1<<PD7))

#define read_MISO() (volatile)((PIND & (1<<PIND6))? HIGH:LOW) // <=> digitalRead(MISO) ĐÚNG
void setup() {
  SPI_SETUP();
  Serial.begin(9600);
}

void loop() {
  uint8_t rev;
  SPI_begin();
  SPI_transfer(0b10100101);
  SPI_transfer(0b00100001);
  SPI_transfer(0b11100001);
  SPI_transfer(0b10100001);
//  SPI_transfer('M');
//  SPI_transfer('S');
//  SPI_transfer('O');
//  SPI_transfer('N');
  //Serial.println("M: "+ String((char)rev));
  SPI_end();
  delay(1000);
//  SPI_begin();
//  char hello[]="helloWorld";
//  for(int i=0;i<10;i++)
//  {
//    SPI_transfer(hello[i]);
//  }
//  SPI_end();
//  delay(1000);
}
void SPI_SETUP()
{
   MOSI_OUTPUT;
   MISO_INPUT;
   write_SCK(0);
   SCK_OUTPUT;
   write_SS(1);
   SS_OUTPUT;
}

void SPI_begin(void)
{
  write_SS(0);
}

void SPI_end(void)
{
  write_SCK(0);
  write_SS(1);
}

// MODE: CPOL=0, CPHASE=0, bitOder=MSB.
uint8_t SPI_transfer(uint8_t byte_out)
{
  uint8_t i=0,x;

  // các biến sử dụng lấy dữ liệu từ MISO
  uint8_t byte_in=0;
  uint8_t ibit=0x80;

  for(i;i<8;i++){
    x=byte_out & 0x80;
    if(x!=0)
    {
      write_MOSI(1);
    }
    else write_MOSI(0);
    // kéo xung SCK lên mức cao
    delayMicroseconds(THalf);
    write_SCK(1);

    if(read_MISO()==1)
    {
      byte_in = byte_in|ibit; // nhận bit MSB
    }

    delayMicroseconds(THalf);
    write_SCK(0);   // Kết thúc một chu kỳ xung SCK

    byte_out<<=1;
    ibit=ibit>>1; // 0x80>>1= 0x01000000=0x40;
  }
  return byte_in;
}
