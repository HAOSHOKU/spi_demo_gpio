// Định nghĩa các chân của slave
#define SCK  4
#define MOSI 5
#define MISO 6
#define SS   7
// cấu hình in/out cho các chân
#define SCK_INPUT    DDRD &= ~(1<<DDD4) // <=> pinMode(SCK_PIN, INPUT);
#define MOSI_INPUT   DDRD &= ~(1<<DDD5)
#define SS_INPUT     DDRD &= ~(1<<DDD7)
#define MISO_OUTPUT  DDRD |= (1<<DDD6) // <=> pinMode(MISO, OUTPUT);
// VIẾT CÁC HÀM MACRO ĐỌC XUẤT CHO CÁC CHÂN
#define read_SCK() ((PIND & (1<<PIND4))? HIGH:LOW) // <=> digitalRead(SCK)
#define read_MOSI() ((PIND & (1<<PIND5))? HIGH:LOW) // <=> digitalRead(MISO)
#define read_SS() ((PIND & (1<<PIND7))? HIGH:LOW) // <=> digitalRead(SS)

#define write_MISO(x) PORTD = x ? PORTD |= (1<<PD6):PORTD &= ~(1<<PD6) //<=> digitalWrite(MISO_HIGH,1);

void setup() {
  SPI_setup();
  Serial.begin(9600);
}

void loop() {
 uint8_t rev;
  //rev=SPI_transfer('S');
  //Serial.println("S:"+ String((char)rev));
  SPI_transfer(0b11010101);
  SPI_transfer(0b10010101);
  SPI_transfer(0x99);
  SPI_transfer(0x55);
//  SPI_transfer('s');
//  SPI_transfer('s');
//  SPI_transfer('o');
//  SPI_transfer('n');

}
void SPI_setup()
{
  SCK_INPUT;
  MOSI_INPUT;
  SS_INPUT; 

  MISO_OUTPUT;
}

uint8_t SPI_transfer(uint8_t byte_out)
{
  uint8_t byte_in=0;
  uint8_t ibit=0x80;

  uint8_t i=0,x;

  while(read_SS()==1);
  for(i;i<8;i++)
  {
    // Write dữ liệu Tại chân MISO
    x= byte_out & 0x80;
    if(x!=0) write_MISO(1);
    else write_MISO(0);
  // read dữ liệu tại chân MOSI  
    while(read_SCK()==0); // đợi SCK==1
    if(read_MOSI()==1) byte_in= byte_in | ibit; 
    // else byte_in = byte_in & (~ibit); // không cần vì đã xét byte_in ban đầu = 0000 0000
    byte_out = byte_out<<1;
    ibit= ibit>>1;
    while(read_SCK()==1); // ket thuc chu ki
  }
  return byte_in;
}
